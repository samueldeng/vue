
var Promise = require('es6-promise').Promise



export default {

	request(method, url, data){
		return new Promise((resolve, reject) => {
				var token = sessionStorage.token
			    axios({
			      method: method,
			      url: url,
			      headers: {'Authorization': 'JWT '+token},
			      data: data
			    })
				.then((response) => {
				    resolve(response)
				})
				.catch((error) => {
				 	reject(error)
				});	
		})
	},




	send(self, method, url){
		if(Object.keys(self.updatedData).length > 0){		 // If nothing's changed then skip 
			var token = sessionStorage.token
	    axios({
	      method: method,
	      url: url,
	      headers: {'Authorization': 'JWT '+token},
	      data: self.updatedData
	    })
	    .then((response) => {
	    	for(let newEach in self.updatedData){
					self[newEach] = self.updatedData[newEach]
				}   // Update Vue data

				self.updatedData = {}  // Empty updatedData object to prevent uploading the same data
	    	self.$emit('saved')
	    	self.unsaved = []
	    })
	    .catch(function (error) {
	    	self.$emit('error')
	    	self.unsaved = []
			console.log(error)
	    });
		}else{
			$('.save').transition('shake')
		}
	}

}

