import Vue from 'vue'
import Router from 'vue-router'

import MeteringRealtime from '@/components/Metering_RealTime'
import MeteringDemand from '@/components/Metering_Demand'
import MeteringEnergy from '@/components/Metering_Energy'
import MeteringIO from '@/components/Metering_IO'
import StatusSystem from '@/components/Status_DeviceInfo'
import SettingsMeter from '@/components/Settings_Meter'
import SettingsCommunications from '@/components/Settings_Communications'
import SettingsManagement from '@/components/Settings_Management'
import SettingsFirmware from '@/components/Settings_Firmware'
import Main from '@/components/main'
import Login from '@/components/login'

Vue.use(Router)

export default new Router({
  mode:"history",
  routes: [
    {
      path: '/',
      redirect: '/metering/realtime',
    },
    {
      path: '/metering',
      component: Main, 
      children: [
        {
          path: 'realtime',
          component: MeteringRealtime
        },
        {
          path: 'demand',
          component: MeteringDemand
        },
        {
          path: 'energy',
          component: MeteringEnergy
        },
        {
          path: 'io',
          component: MeteringIO
        }         
      ]
    },

    {
      path: '/settings',
      component: Main, 
      children: [
        {
          path: 'meter',
          component: SettingsMeter
        },
        {
          path: 'communications',
          component: SettingsCommunications
        },
        {
          path: 'management',
          component: SettingsManagement
        },
        {
          path: 'firmware',
          component: SettingsFirmware
        }          
      ]
    },

    {
      path: '/status',
      component: Main, 
      children: [
        {
          path: 'system',
          component: StatusSystem
        },
        {
          path: 'alarm',
          component: SettingsCommunications
        },
        {
          path: 'systemevent',
          component: SettingsManagement
        }        
      ]
    },

    {
      path: '/login',
      name: 'login',
      component: Login
    }    
  ]
})
