import Vue from 'vue'
import Vuex from 'vuex'

import Conf from '../conf/conf'
var host = Conf.host
var meterName = Conf.meterName
var RESTApi = '/api/meters/' + meterName + '/settings/communications/'

Vue.use(Vuex)

// Use modules for Vuex
const wifi = {
	state: {
	  		pageLoading:true,
	  		unsaved: [],
	  		originalData: {},
	  		updatedData: {},
	  		validationError: [],
	  		wifiModes: ['Access Point', 'Station'],

	  		// DB fields
	  		enable:'',
	  		mode: '',
	  		wifiModePreview: '',
	  		apConfigssid: '',
	  		apConfigip: '',
	  		apConfigkey: '',
	  		staConfigdhcpEnable: '',
	  		staConfiggateway: '',
	  		staConfigip: '',
	  		staConfigkey: '',
	  		staConfigssid: '',
	  		staConfigsubnet: ''
	},

	getters: {
		wifiMode: state => state.wifiMode
	},

	mutations: {
		getData: (state, response) => {
			state.originalData = response.data.wifi
	      	state.enable = response.data.wifi.enable
	      	state.mode = state.wifiModePreview = response.data.wifi.mode
	      	state.apConfigssid = response.data.wifi.apConfig.ssid
	      	state.apConfigip = response.data.wifi.apConfig.ip
	      	state.apConfigkey = response.data.wifi.apConfig.key
	      	state.staConfigdhcpEnable = response.data.wifi.staConfig.dhcpEnable
	      	state.staConfiggateway = response.data.wifi.staConfig.gateway
	      	state.staConfigip = response.data.wifi.staConfig.ip
	      	state.staConfigkey = response.data.wifi.staConfig.key
	      	state.staConfigssid = response.data.wifi.staConfig.ssid
	      	state.staConfigsubnet = response.data.wifi.staConfig.subnet
		}
	},

	actions: {    // async 
		getData: context => {
			// var token = sessionStorage.token
			var url = '/api/meters/' + meterName + '/settings/communications/wifi'
		    axios({
				method: 'get',
				url: url
				// headers: {'Authorization': 'JWT '+token},
		    })
		    .then(function (response) {
	      		context.commit('getData', response)
		    })
		    .catch(function (error) {
		      console.log(error);
		    }); 
		}
	}
}

export const store = new Vuex.Store({
  modules: {
    wifi: wifi
  }
})

//// NOT Use modules for Vuex
// export const store = new Vuex.Store({
// 	state: {
// 		wifi: {
// 	  		pageLoading:true,
// 	  		updatedData: {},
// 	  		wifiModes: ['Access Point', 'Station'],

// 	  		// DB fields
// 	  		wifiEnable:'',
// 	  		wifiMode: '',
// 	  		ssid: '',
// 	  		networkKey: '',
// 	  		ip: '',
// 	  		dhcpDNSServer1: '',
// 	  		dhcpDNSServer2: ''
// 	  	}
// 	},

// 	getters: {
// 		wifiMode: state => state.wifi.wifiMode
// 	},

// 	mutations: {
// 		getData: (state, response) => {
// 			var data = response.data
// 	      	state.wifi.wifiEnable = data.wifiEnable
// 	      	state.wifi.wifiMode = data.wifiMode
// 	      	state.wifi.ssid = data.ssid
// 	      	state.wifi.networkKey = data.networkKey
// 	      	state.wifi.ip = data.wifi_IPAddress
// 	      	state.wifi.dhcpDNSServer1 = data.dhcpDNSServer1
// 	      	state.wifi.dhcpDNSServer2 = data.dhcpDNSServer2	
// 		}
// 	},

// 	actions: {    // async 
// 		getData: context => {
// 			var token = sessionStorage.token
// 			var url = '/api/meters/' + meterName + '/settings/communications/'
// 		    axios({
// 				method: 'get',
// 				url: url,
// 				headers: {'Authorization': 'JWT '+token},
// 		    })
// 		    .then(function (response) {
// 	      		context.commit('getData', response)
// 		    })
// 		    .catch(function (error) {
// 		      console.log(error);
// 		    }); 
// 		}
// 	}
// })
