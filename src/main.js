import 'babel-polyfill'
import Vue from 'vue'
import ECharts from 'vue-echarts/components/ECharts.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en'
import semantic from 'semantic'
import calendar from 'semantic-ui-calendar/dist/calendar.min.js'
import '../node_modules/semantic-ui-calendar/dist/calendar.min.css'
import '../node_modules/semantic-ui-css/semantic.min.css'
import "datatables.net"
import "datatables.net-se"
import "datatables.net-buttons"
import "datatables.net-buttons-se"
import '../node_modules/datatables.net-se/css/dataTables.semanticui.css'
import App from './App'
import router from './router'

import Conf from './conf/conf'
import { store } from './store/store'

// import axios from 'axios';
// Vue.prototype.$axios = axios
Vue.component('chart', ECharts)

Vue.config.productionTip = false
Vue.use(ElementUI, { locale })

var host = Conf.host
// router.beforeEach((to, from, next) => {   // Router defender
// 	if(!sessionStorage.token){
// 		if (to.path !== '/login') {
// 	      	next({path: '/login'});    //Equals: another router.beforeEach(); Reseeeet the path to 'login'
//         }else {
//             next();                    //Enter the route, with the path of 'login'
//         }
// 	}else{
//         axios({
//           method: 'post',
//           url: '/api/auth/token/verify/',
//           data: {"token": sessionStorage.token}
//         })
//         .then(function (response) {
//           next();
//         })
//         .catch(function (error) {
//           sessionStorage.clear();
//           next({path: '/login'}); 
//         });    
// 	}
// })

new Vue({
  store: store,
  el: '#app',
  router: router,
  template: '<App/>',
  components: { App }
})
