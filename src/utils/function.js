var Promise = require('es6-promise').Promise


export default {

	request(method, url, data){
		return new Promise((resolve, reject) => {
			// var token = sessionStorage.token
		    axios({
		      method: method,
		      url: url,
		      // headers: {'Authorization': 'JWT '+token},
		      data: data
		    })
			.then((response) => {
			    resolve(response)
			})
			.catch((error) => {
			 	reject(error)
			});	
		})
	},

	send(self, method, url){
		if(Object.keys(self.updatedData).length > 0){		 // If nothing's changed then skip 
			// var token = sessionStorage.token 
			self.$emit('loaderOn')
			const mutate = (keyList, value, obj) => {  // keyList: ['eth0', 'dhcpEnable']
				if(keyList.length > 1){
					for(let objKey in obj){
						if(objKey == keyList[0]){
							keyList.shift()     // pop the first key out
							mutate(keyList, value, obj[objKey])
						}
					}
				}else{
					obj[keyList[0]] = value
				}
			}

			// For datalog page(Array with 3 logger objects); Locate the loggerID first and modify the content
			if(self.originalData instanceof Array){
				for(let i in self.updatedData){    
					var keyList = i.split('.')
					var re = /\d+/
					var loggerID = re.exec(keyList[0])[0]
					var arrayIndex = ''
					for(let index in self.originalData){
						self.originalData[index].loggerId == loggerID ? arrayIndex = index : {}
					}
					keyList.shift()
					mutate(keyList, self.updatedData[i], self.originalData[arrayIndex])
					// console.log(self.originalData[arrayIndex])
				}
			}else{
				for(let i in self.updatedData){    
					var keyList = i.split('.')
					var obj = {}
					mutate(keyList, self.updatedData[i], self.originalData)
				}
			}
				console.log(self.originalData)

		    axios({
		      method: method,
		      url: url,
		      // headers: {'Authorization': 'JWT '+token},
		      data: self.originalData
		    })
		    .then((response) => {
		    	for(let newEach in self.updatedData){
		    			var keyList = newEach.split('.')
		    			mutate(keyList, self.updatedData[newEach], self)
					}   // Update Vue data

				self.updatedData = {}  // Empty updatedData object to prevent uploading the same data
				setTimeout(()=>{self.$emit('loaderOff')}, 500)
				setTimeout(()=>{self.$emit('saved')}, 500)		
		    	self.unsaved = []
		    })
		    .catch(function (error) {
		    	self.$emit('loaderOff')
		    	self.$emit('error')
		    	self.unsaved = []
				console.log(error)
		    });
		}else{
			$('.save').transition('shake')
		}
	},

  	update(self, name, value, url){
  		self.updatedData[name] = value
  		var index = self.unsaved.indexOf(name)
  		if (index == -1) {
		    self.unsaved.push(name)
		    self.$emit('changed')
		}
		self.$emit('updatedData', self.updatedData, url)
  	},

  	clean(self, name){
  		var index = self.unsaved.indexOf(name)
  		if (index > -1) {
		    self.unsaved.splice(index, 1)
		    self.$emit('unchanged')
		}
  		delete self.updatedData[name]
  	},

}

